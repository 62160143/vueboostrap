module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/~cs62160143/learn_bootstrap/'
    : '/'
}
